export const useStyles = (theme) => ({
  root: {
    padding: 10,
    backgroundColor: "#292929",
    color: "#fff",
    minHeight: "100vh",
  },
});
