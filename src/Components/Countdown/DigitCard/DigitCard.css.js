export const useStyles = (theme) => ({
  paper: {
    padding: 10,
    fontSize: 30,
    fontWeight: "bolder",
    borderRadius: 10,
    boxShadow: "0 4px 10px 0 rgba(0,0,0,.25)",
  },
});
