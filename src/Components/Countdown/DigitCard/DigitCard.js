import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";

import { useStyles } from "./DigitCard.css";

class DigitCard extends Component {
  render() {
    const { classes } = this.props;
    return (
      <Paper elevation={2} className={classes.paper}>
        {this.props.digit}
      </Paper>
    );
  }
}

export default withStyles(useStyles)(DigitCard);
