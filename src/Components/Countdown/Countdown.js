import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import moment from "moment";

import { useStyles } from "./Countdown.css";
import DigitCard from "./DigitCard/DigitCard";

class Countdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timer: 0,
      time: {},
      seconds: 0,
      timeOut: false,
    };
  }

  componentDidMount() {
    let start = moment(new Date(), "DD-MM-YYYY HH:mm");
    let end = moment("2021-07-12 23:39");
    let days = end.diff(start, "days");
    let hours = end.diff(start, "hours");
    let minutes = end.diff(start, "minutes");
    let tempSeconds = end.diff(start, "seconds");
    console.log(days, hours, minutes, tempSeconds);
    if (days < 0 || hours < 0 || minutes < 0 || tempSeconds < 0) {
      let timeLeft = {
        d: "00",
        h: "00",
        m: "00",
        s: "00",
      };
      this.setState({
        time: timeLeft,
        seconds: -1,
        timeOut: true,
      });
    } else {
      let timeLeft = {
        d: days < 10 ? "0" + days : days,
        h: hours % 24 < 10 ? "0" + (hours % 24) : hours % 24,
        m: minutes % 60 < 10 ? "0" + (minutes % 60) : minutes % 60,
        s: tempSeconds % 60 < 10 ? "0" + (tempSeconds % 60) : tempSeconds % 60,
      };
      this.setState({
        time: timeLeft,
        seconds: tempSeconds >= 0 ? tempSeconds : 0,
        timeOut: false,
      });
      this.startTimer();
    }
  }

  secondsToTime = (secs) => {
    let days = Math.floor(secs / (60 * 60 * 24));

    let divisor_for_hours = secs % (60 * 60 * 24);
    let hours = Math.floor(divisor_for_hours / (60 * 60));

    let divisor_for_minutes = secs % (60 * 60);
    let minutes = Math.floor(divisor_for_minutes / 60);

    let divisor_for_seconds = divisor_for_minutes % 60;
    let seconds = Math.ceil(divisor_for_seconds);

    let obj = {
      d: days < 10 ? "0" + days : days,
      h: hours < 10 ? "0" + hours : hours,
      m: minutes < 10 ? "0" + minutes : minutes,
      s: seconds < 10 ? "0" + seconds : seconds,
    };
    return obj;
  };

  startTimer = async () => {
    if (this.state.timer === 0 && this.state.timeOut === false) {
      this.setState({ timer: setInterval(this.countDown, 1000) });
    }
  };

  countDown = () => {
    // Remove one second, set state so a re-render happens.
    let seconds = this.state.seconds - 1;
    this.setState({
      time: this.secondsToTime(seconds),
      seconds: seconds,
    });

    // Check if we're at zero.
    if (seconds === 0) {
      clearInterval(this.state.timer);
      this.setState({ timeOut: true });
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <div style={{ textAlign: "center", fontSize: 50 }}>I am coming</div>
        <div className={classes.body}>
          <div className={classes.content}>
            <DigitCard digit={this.state.time.d} />
            <div>{this.state.time.d < 2 ? "Day" : "Days"}</div>
          </div>
          <div className={classes.content}>
            <DigitCard digit={this.state.time.h} />
            <div>{this.state.time.h < 2 ? "Hour" : "Hours"}</div>
          </div>
          <div className={classes.content}>
            <DigitCard digit={this.state.time.m} />
            <div>{this.state.time.m < 2 ? "Minute" : "Minutes"}</div>
          </div>
          <div className={classes.content}>
            <DigitCard digit={this.state.time.s} />
            <div>{this.state.time.s < 2 ? "Second" : "Seconds"}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(useStyles)(Countdown);
