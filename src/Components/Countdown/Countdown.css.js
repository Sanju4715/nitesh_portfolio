export const useStyles = (theme) => ({
  root: {
    minHeight: "100vh",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#181818",
    color: "#fff",
  },
  body: { display: "flex", flexDirection: "row" },
  content: { padding: 10, textAlign: "center" },
});
